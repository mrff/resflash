#!/bin/sh

# Unmount a mounted resflash image or filesystem
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit -o nounset -o pipefail #-o xtrace # DEBUG

. $(dirname ${0})/resflash.sub

umount_all

rm -r /tmp/resflash.??????
